import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/Sklep',
    name: 'Sklep',
    component: () => import('../views/Sklep.vue')
  },
  {
    path: '/Donate',
    name: 'Donate',
    component: () => import('../views/Donate.vue')
  },
  {
    path: '/Kontakt',
    name: 'Kontakt',
    component: () => import('../views/Kontakt.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
